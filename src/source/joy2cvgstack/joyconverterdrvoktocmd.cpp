#include "joy2cvgstack/joyconverterdrvoktocmd.h"

// Values for Mikrokopter Oktokopter
#define LL_AUTOPILOT_COMMAND_TILT_MIN_VALUE   ( -63.50)    // deg      int_min: -2047
#define LL_AUTOPILOT_COMMAND_TILT_MAX_VALUE   ( +111.40)    // deg      int_max:  2047
#define LL_AUTOPILOT_COMMAND_DYAW_MIN_VALUE   (-127.00)    // deg/s    int_min: -2047
#define LL_AUTOPILOT_COMMAND_DYAW_MAX_VALUE   (+195.00)    // deg/s    int_max:  2047
#define LL_AUTOPILOT_COMMAND_THRUST_MIN_VALUE (   0.00)    // N        int_min:     0
#define LL_AUTOPILOT_COMMAND_THRUST_MAX_VALUE ( +49.05)    // N        int_max:  4095

JoyConverterDrvOktoCmd::JoyConverterDrvOktoCmd() : DroneModule(droneModule::active)
{
    init();
    return;
}

JoyConverterDrvOktoCmd::~JoyConverterDrvOktoCmd()
{

    return;
}

void JoyConverterDrvOktoCmd::init()
{
    return;
}

void JoyConverterDrvOktoCmd::close()
{
    return;
}

void JoyConverterDrvOktoCmd::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    OutputPubl = n.advertise<droneMsgsROS::droneAutopilotCommand>("command/low_level", 1, true);


    //Subscriber
    InputSubs=n.subscribe("joy", 1, &JoyConverterDrvOktoCmd::inputCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool JoyConverterDrvOktoCmd::resetValues()
{
    return true;
}

//Start
bool JoyConverterDrvOktoCmd::startVal()
{
    return true;
}

//Stop
bool JoyConverterDrvOktoCmd::stopVal()
{
    return true;
}

//Run
bool JoyConverterDrvOktoCmd::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

//#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void JoyConverterDrvOktoCmd::inputCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

//  # Received input from joypad "Thrustmaster, Dual Analog 3", note that this
//  #   joypad is very low quality, don't fly the Pelican with it!!!!
//  #     msg->axes[0] // stick: left -horizontal, left:+1 right:-1 //   dyaw
//  #     msg->axes[1] // stick: left -vertical  ,   up:+1  down:-1 // thrust
//  #     msg->axes[2] // stick: right-horizontal, left:+1 right:-1 //   roll
//  #     msg->axes[3] // stick: right-vertical  ,   up:+1  down:-1 //  pitch
//  std::cout << " pitch:" << msg->axes[3] << std::endl;
//  std::cout << "  roll:" << msg->axes[2] << std::endl;
//  std::cout << "  dyaw:" << msg->axes[0] << std::endl;
//  std::cout << "thrust:" << msg->axes[1] << std::endl;

//  # Desired joypad-vs-pelican behaviour
//  #   signs in the code are assigned to obtain the desired behaviour.
//  #   The yaw sign test was not done during flight, so it might be wrong.
//  #     stick: left -horizontal, left:      dyaw-leftwards   right:        dyaw-rightwards
//  #     stick: left -vertical  ,   up:     thrust-upwards     down:     thrust-downwards
//  #     stick: right-horizontal, left: horizontal-left       right: horizontal-left
//  #     stick: right-vertical  ,   up: horizontal-frontwards  down: horizontal-backwards

    droneMsgsROS::droneAutopilotCommand OutputMsgs;

    OutputMsgs.pitch  = (float) (+1.0)*(  msg->axes[1] * LL_AUTOPILOT_COMMAND_TILT_MAX_VALUE);
    OutputMsgs.roll   = (float) (-1.0)*(  msg->axes[0] * 117.59 );
    OutputMsgs.dyaw   = (float) (-1.0)*(  msg->axes[5] * LL_AUTOPILOT_COMMAND_DYAW_MAX_VALUE );
    OutputMsgs.thrust = (float) (+1.0)*( (msg->axes[2] + 0.7)/1.25 * LL_AUTOPILOT_COMMAND_THRUST_MAX_VALUE);

//    // Motors power on test:
//    if (OutputMsgs.thrust > 1000) {
//        OutputMsgs.yaw    = (float) (   0);
//        OutputMsgs.thrust = (float) (   0);
//    } else {
//        OutputMsgs.yaw    = (float) ( 1.0);
//        OutputMsgs.thrust = (float) ( 0.0);
//    }

    OutputMsgs.header.stamp = ros::Time::now();

    //Publish
    OutputPubl.publish(OutputMsgs);
    return;
}
